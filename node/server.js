const url = require('url');
const WebSocket = require('ws');
const amqplib = require('amqplib/callback_api');
const wss = new WebSocket.Server({ port: 3333 });
const amqpUrl = 'amqp://guest:guest@rabbitmq';

let amqpConnect = null;
let amqpChannel = null;

amqplib.connect(amqpUrl, (err, conn) => {
	process.once('SIGINT', () => { conn.close(); });
	amqpConnect = conn;
	conn.createChannel((err, ch) => {
		amqpChannel = ch;
	});
});

wss.on('connection', (ws, req) => {
	const queryObject = url.parse(req.url, true).query;
	let exchange = 'userRegisterResult';
	amqpChannel.assertExchange(exchange, 'fanout', {durable: true});

	amqpChannel.assertQueue('', {exclusive: true}, (error2, q) => {
		amqpChannel.bindQueue(q.queue, exchange, '');

		amqpChannel.consume(q.queue, (msg) => {
			let content = msg.content.toString();
			let json = JSON.parse(content);
			if (queryObject.hash === json.hash) {
				ws.send(content);
			}
		}, { noAck: true });
	});
});