<?php

namespace App\Service\User;

use App\Entity\User;

interface UserServiceInterface
{
	public function create(User $user) : bool;
	public function findOneByUsername(string $username) : ?User;
}