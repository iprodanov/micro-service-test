<?php

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\Encryption\BCryptService;

class UserService implements UserServiceInterface
{
	private $userRepository;
	private $encryptionService;

	public function __construct(UserRepository $userRepository, BCryptService $encryptionService)
	{
		$this->userRepository = $userRepository;
		$this->encryptionService = $encryptionService;
	}

	/**
	 * @param User $user
	 * @return bool
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function create(User $user): bool
	{
		$passwordHash = $this->encryptionService->hash($user->getPassword());
		$user->setPassword($passwordHash);

		return $this->userRepository->insert($user);
	}

	/**
	 * @param string $username
	 * @return User|null|object
	 */
	public function findOneByUsername(string $username): ?User
	{
		return $this->userRepository->findOneBy(['username' => $username]);
	}
}