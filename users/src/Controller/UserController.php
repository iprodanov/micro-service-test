<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;

class UserController extends AbstractController
{
	/**
	 * @var SerializerInterface
	 */
	private $serializer;

	public function __construct(SerializerInterface $serializer)
	{
		$this->serializer =  $serializer;
	}

    /**
     * @Route("/", name="users")
     */
    public function showUsersAction(): JsonResponse
	{
    	$users = $this->getDoctrine()->getRepository(User::class)->findAll();
		$json = $this->serializer->serialize($users, 'json');

		return new JsonResponse($json,200, [], true);
    }
}
