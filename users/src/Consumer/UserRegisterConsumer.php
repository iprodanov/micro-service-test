<?php

namespace App\Consumer;

use Exception;
use App\Entity\User;
use App\Service\User\UserServiceInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class UserRegisterConsumer implements ConsumerInterface
{
	/**
	 * @var UserServiceInterface
	 */
	private $userService;

	/**
	 * @var ProducerInterface
	 */
	protected $producer;

	public function __construct(UserServiceInterface $userService, ProducerInterface $userRegisterProducer)
	{
		$this->userService = $userService;
		$this->producer = $userRegisterProducer;
	}

	public function execute(AMQPMessage $msg)
	{
		$data = json_decode($msg->body, true);
		$user = new User();
		$message = [];

		$user->setUsername($data['username']);
		$user->setPassword($data['password']);

		try {
			if ($this->userService->create($user)) {
				$user = $this->userService->findOneByUsername($data['username']);
				$message = [
					'status' => 'success',
					'hash' => $data['hash'],
					'data' => [
						'id' => $user->getId(),
						'username' => $user->getUsername(),
						'password' => $user->getPassword()
					]
				];
			}
		} catch(Exception $e) {
			$message = ['status' => 'failed', 'hash' => $data['hash'], 'message' => $e->getMessage()];
		}

		$json = json_encode($message);
		$this->producer->setContentType('application/json');
		$this->producer->publish($json);
		echo $json . PHP_EOL;
	}
}