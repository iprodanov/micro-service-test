const WS = {
	ws: undefined,
	reader: new FileReader(),

	setupConnection() {
		let hash = getCookie('hash');

		if (hash) {
			this.initiateWebsocket(hash);
		} else {
			// Generate UUID v4 hash and save in session cookie
			hash = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));
			//document.cookie = 'hash=' + hash;
			setCookie('hash', hash);
			this.initiateWebsocket(hash);
		}
	},

	initiateWebsocket(hash) {
		this.ws = new WebSocket('ws://' + location.hostname + ':3333?hash=' + hash);

		this.ws.onmessage = (event) => {
			console.log(event.data);
			let json = JSON.parse(event.data);

			if (json.status === 'success') {
				displayMessage('Registered successfully.', 'success');
			} else {
				displayMessage('Something went wrong, please try again.', 'danger');
			}
		};

		this.ws.onerror = () => {
			console.log('WebSocket error');
		};

		this.ws.onopen = () => {
			console.log('WebSocket connection established');
		};

		this.ws.onclose = () => {
			console.log('WebSocket connection closed');
		};
	},
};

WS.setupConnection();

if (document.getElementById("register")) {
	document.querySelector("#register").addEventListener('submit', function(e) {
		e.preventDefault();

		let formData = {
			username: document.getElementById('username').value,
			password: document.getElementById('password').value,
			confirmPassword: document.getElementById('confirmPassword').value,
			hash: getCookie('hash')
		};

		displayMessage('Loading ...', 'info');

		postData('http://localhost:8080/users', formData)
		.then(function(data) {
			if (data.status === 'failed') {
				displayMessage(data.message, 'danger');
			}
		});
	});
}

if (document.getElementById("login")) {
	document.querySelector('#login').addEventListener('submit', function(e) {
		e.preventDefault();

		let formData = {
			username: document.getElementById('username').value,
			password: document.getElementById('password').value,
			hash: getCookie('hash')
		};

		displayMessage('Loading ...', 'info');

		postData('http://localhost:8080/token', formData)
		.then(function(data) {
			if (data.status === 'success') {
				setCookie('token', data.token, 1);
			}

			displayMessage(data.message, (data.status === 'failed' ? 'danger' : 'success'));
		});
	});
}

async function postData(url = '', data = {}) {
	const response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		body: encodeFormData(data)
	});
	
	return await response.json();
};

function encodeFormData(parm) {
	return Object.keys(parm).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(parm[key])).join('&');
}

function displayMessage(message, type) {
	document.getElementById('message').innerHTML = '<div class="alert alert-' + type + ' mt-2">' + message + '</div>';
}

function getCookie(name) {
	let value = '; ' + document.cookie;
	let parts = value.split('; ' + name + '=');

	if (parts.length == 2) {
		return parts.pop().split(';').shift();
	}
}

function setCookie(name, value, hours) {
	let expires = '';

	if (hours) {
		let date = new Date();
		date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
		expires = '; expires=' + date.toUTCString();
	}

	document.cookie = name + '=' + (value || '')  + expires + '; path=/';
}