<?php

namespace App\Controller;

use App\Exception\UserException;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Users\UserServiceInterface;

class UserController extends AbstractController
{
	/**
	 * @var UserServiceInterface
	 */
	private $userService;
	/**
	 * @var ProducerInterface
	 */
	protected $producer;

	public function __construct(UserServiceInterface $userService, ProducerInterface $userRegisterProducer)
	{
		$this->userService = $userService;
		$this->producer = $userRegisterProducer;
	}

	/**
	 * @Route("/users", name="register_user", methods={"POST"})
	 * @param Request $request
	 * @return Response
	 * @throws \Psr\Cache\InvalidArgumentException
	 */
	public function registerUserAction(Request $request): Response
	{
		$username = $request->request->get('username');
		$password = $request->request->get('password');
		$confirmPassword = $request->request->get('confirmPassword');
		$hash = $request->request->get('hash');

		try {
			$this->userService->registerValidation($username, $password, $confirmPassword);
			$this->producer->setContentType('application/json');
			$this->producer->publish(json_encode(['username' => $username, 'password' => $password, 'hash' => $hash]));

			return new Response(
				json_encode(['status' => 'WaitingConfirmation']),
				Response::HTTP_OK,
				['content-type' => 'application/json']
			);
		} catch(UserException $exception) {
			return new Response(
				json_encode([
					'status' => 'failed',
					'error' => $exception->getCustomCode(),
					'message' => $exception->getMessage()
				]),
				Response::HTTP_BAD_REQUEST,
				['content-type' => 'application/json']
			);
		}
	}

	/**
	 * @Route("/token", name="get_token", methods={"POST"})
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function getTokenAction(Request $request): Response
	{
		$username = $request->request->get('username');
		$password = $request->request->get('password');

		try {
			return new Response(
				json_encode([
					'status' => 'success',
					'token' => $this->userService->authentication($username, $password),
					'message' => 'Successfully logged in.'
				]),
				Response::HTTP_OK,
				['content-type' => 'application/json']
			);
		} catch(UserException $exception) {
			return new Response(
				json_encode([
					'status' => 'failed',
					//'error' => 'Unauthorized',
					'error' => $exception->getCustomCode(),
					'message' => $exception->getMessage()
				]),
				Response::HTTP_BAD_REQUEST,
				['content-type' => 'application/json']
			);
		}
    }
}
