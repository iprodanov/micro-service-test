<?php

namespace App\Exception;

class UserException extends \Exception
{
	private $customCode;

	public function setCustomCode($code): void
	{
		$this->customCode = $code;
	}

	public function getCustomCode(): string
	{
		return $this->customCode;
	}
}