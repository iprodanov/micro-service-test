<?php

namespace App\Service\Users;

use App\Exception\UserException;
use DateInterval;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUser;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use App\Service\Encryption\BCryptService;

class UserService implements UserServiceInterface
{
	/**
	 * @var AdapterInterface
	 */
	private $cache;
	/**
	 * @var BCryptService
	 */
	private $encryptionService;
	/**
	 * @var JWTTokenManagerInterface
	 */
	private $JWTManager;

	public function __construct(AdapterInterface $cache, BCryptService $encryptionService, JWTTokenManagerInterface $JWTManager)
	{
		$this->cache = $cache;
		$this->encryptionService = $encryptionService;
		$this->JWTManager = $JWTManager;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $confirmPassword
	 * @return void
	 * @throws UserException
	 * @throws \Psr\Cache\InvalidArgumentException
	 */
	public function registerValidation(string $username, string $password, string $confirmPassword): void
	{
		if (!preg_match('/^[a-zA-Z0-9]{5,}$/', $username)) {
			$exception = new UserException('Please enter a valid username.');
			$exception->setCustomCode('InvalidUsernameException');

			throw $exception;
		}

		if (empty($password) || $password !== $confirmPassword) {
			$exception = new UserException('Password is empty or mismatch.');
			$exception->setCustomCode('PasswordMismatchException');

			throw $exception;
		}

		$item = $this->cache->getItem('user.' . $username);

		if ($item->isHit()) {
			$exception = new UserException('User with username "' . $username . '" already exists.');
			$exception->setCustomCode('UsernameAlreadyExistsException');

			throw $exception;
		}
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @return string
	 * @throws UserException
	 * @throws \Psr\Cache\InvalidArgumentException
	 */
	public function authentication(string $username, string $password): string
	{
		$item = $this->cache->getItem('user.' . $username);

		if (!$item->isHit()) {
			$exception = new UserException('Username not exist.');
			$exception->setCustomCode('Unauthorized');

			throw $exception;
		}

		if (!$this->encryptionService->verify($password, $item->get()['password'])) {
			$exception = new UserException('Password not match.');
			$exception->setCustomCode('Unauthorized');

			throw $exception;
		}

		$tokenCache = $this->cache->getItem('token.' . $username);

		if ($item->isHit()) {
			$payload = JWTUser::createFromPayload($item->get()['username'], $item->get());
			$token = $this->JWTManager->create($payload);
			$tokenCache->set($token);
			$tokenCache->expiresAfter(new DateInterval('PT1H')); // the item will be cached for 1 hour
			$this->cache->save($tokenCache);
		} else {
			$token = $tokenCache->get();
		}

		return $token;
	}
}