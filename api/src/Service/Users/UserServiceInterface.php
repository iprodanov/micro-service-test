<?php

namespace App\Service\Users;

interface UserServiceInterface
{
	public function registerValidation(string $username, string $password, string $confirmPassword): void;
	public function authentication(string $username, string $password): string;
}