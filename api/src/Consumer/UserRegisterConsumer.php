<?php

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class UserRegisterConsumer implements ConsumerInterface
{
	/**
	 * @var AdapterInterface
	 */
	protected $cache;

	public function __construct(AdapterInterface $cache)
	{
		$this->cache = $cache;
	}

	public function execute(AMQPMessage $msg)
	{
		$data = json_decode($msg->body, true);

		if ($data['status'] === 'success') {
			echo $data['data']['username'] . PHP_EOL;

			$item = $this->cache->getItem('user.' . $data['data']['username']);

			if (!$item->isHit()) {
				$item->set($data['data']);
				$this->cache->save($item);
			}
		}
	}
}