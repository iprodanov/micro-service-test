<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{

	/**
	 * @test
	 */
	public function try_to_register_user_with_wrong_confirm_password()
	{
		$client = static::createClient();

		$client->request('POST', '/users', ['username' => 'Test', 'password' => 'pass', 'confirmPassword' => 'wrong_pass']);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	/**
	 * @test
	 */
	public function try_to_login_with_non_exist_user()
	{
		$client = static::createClient();

		$client->request('POST', '/token', ['username' => 'Test', 'password' => 'pass']);

		$this->assertEquals(400, $client->getResponse()->getStatusCode());
	}

	/**
	 * @test
	 */
	public function login_with_exist_user()
	{
		$client = static::createClient();

		$client->request('POST', '/token', ['username' => 'Test', 'password' => 'pass']);

		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}
}